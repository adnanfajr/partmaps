package io.github.partmaps.partmaps;

import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class AddPartnerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_partner);

        TabLayout tab = (TabLayout) findViewById(R.id.tab_add_partner);
        tab.addTab(tab.newTab().setText("Search by ID"));
        tab.addTab(tab.newTab().setText("Search by Email"));

        ListView list = (ListView) findViewById(R.id.list_add_partner);
        String[] daftar = new String[] {
                "Alvin Rahman Kautsar (pending)",
                "Noptrina Puspitasari (pending)",
                "Shaqilla Azzahra (pending)",
                "Achmad Affandi (pending)"
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, android.R.id.text1, daftar
        );
        list.setAdapter(adapter);

    }
}
