package io.github.partmaps.partmaps;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * A placeholder fragment containing a simple view.
 */
public class GroupsFragment extends Fragment {

    //public GroupsFragment() {
    //}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_groups, container, false);

        ListView _group = (ListView) view.findViewById(R.id.listgroup);
        String[] content = new String[]{"GEMASTIK", "DEVCOMP", "PIMNAS"};
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
                (this.getContext(), android.R.layout.simple_list_item_1, android.R.id.text1, content);
        _group.setAdapter(arrayAdapter);

        _group.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(getActivity().getApplicationContext(), MapsActivity.class));
            }
        });

        FloatingActionButton fab_group = (FloatingActionButton) view.findViewById(R.id.fab_group);
        fab_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();
                Intent intent = new Intent(getActivity().getApplicationContext(),
                        CreateGroupActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }
}
