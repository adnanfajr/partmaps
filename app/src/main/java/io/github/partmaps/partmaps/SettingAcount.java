package io.github.partmaps.partmaps;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class SettingAcount extends Activity {

    static int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_acount);

    }

    public void check(){
        if (counter == -3) {
            Toast.makeText(this, "TOLONG! Ada BEGAL!", Toast.LENGTH_SHORT).show();
            counter = 0;
        } else if (counter == -2) {
            Toast.makeText(this, "TOLONG! Ada MALING SANDAL!", Toast.LENGTH_SHORT).show();
            counter = 0;
        } else if (counter == 1) {
            Toast.makeText(this, "TOLONG!!", Toast.LENGTH_SHORT).show();
            counter = 0;
        }
        else {
            //
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == android.view.KeyEvent.KEYCODE_VOLUME_DOWN) {
            counter--;
            check();
            //Toast.makeText(this, "Volume Down Pressed", Toast.LENGTH_SHORT).show();
            return true;
        }
        else if (keyCode == android.view.KeyEvent.KEYCODE_VOLUME_UP) {
            counter++;
            check();
            //Toast.makeText(this, "Volume Up Pressed", Toast.LENGTH_SHORT).show();
            return true;
        }
        else {
            return super.onKeyDown(keyCode, event);
        }
    }
}
