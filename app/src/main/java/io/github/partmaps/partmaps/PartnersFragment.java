package io.github.partmaps.partmaps;

import android.app.Dialog;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * A placeholder fragment containing a simple view.
 */
public class PartnersFragment extends Fragment {

    public PartnersFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_partners, container, false);

        ListView list = (ListView) view.findViewById(R.id.listpart);
        String[] daftar = new String[] {
                "Alvin",
                "Rahman",
                "Kautsar",
                "Noptrina",
                "Puspitasari",
                "Shaqilla",
                "Azzahra",
                "Achmad",
                "Affandi"
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this.getContext(), android.R.layout.simple_list_item_1, android.R.id.text1, daftar
        );
        list.setAdapter(adapter);

        FloatingActionButton fab_part = (FloatingActionButton) view.findViewById(R.id.fab_part);
        fab_part.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();
                Intent intent = new Intent(getActivity().getApplicationContext(),
                        AddPartnerActivity.class);
                startActivity(intent);
            }
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                // Create custom dialog object
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                // Include dialog.xml file
                dialog.setContentView(R.layout.dialog_contact);
                // set values for custom dialog components - text, image and button
                dialog.show();
            }
        });

        return view;
    }
}
