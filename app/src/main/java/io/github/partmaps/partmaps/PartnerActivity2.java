package io.github.partmaps.partmaps;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class PartnerActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partner2);

        ListView _list = (ListView) findViewById(R.id.list2invite);
        String[] daftar = new String[] {
                "Alvin",
                "Rahman",
                "Kautsar",
                "Noptrina",
                "Puspitasari",
                "Shaqilla",
                "Azzahra",
                "Achmad",
                "Affandi"
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, android.R.id.text1, daftar
        );
        _list.setAdapter(adapter);
    }
}
