package io.github.partmaps.partmaps;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class PartnerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partner);

        Button _add = (Button) findViewById(R.id.btn_addpartner);
        _add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PartnerActivity.this, AddPartnerActivity.class));
            }
        });

        ListView list = (ListView) findViewById(R.id.list_partner);
        String[] daftar = new String[] {
                "Alvin",
                "Rahman",
                "Kautsar",
                "Noptrina",
                "Puspitasari",
                "Shaqilla",
                "Azzahra",
                "Achmad",
                "Affandi"
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, android.R.id.text1, daftar
        );
        list.setAdapter(adapter);
    }
}
