package io.github.partmaps.partmaps;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.SearchView;
import android.support.v7.app.ActionBarActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    static int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        final TextView searchview = (TextView) findViewById(R.id.searchView1);

        Button search = (Button) findViewById(R.id.btn_search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String g = searchview.getText().toString();
                Geocoder geocoder = new Geocoder(getBaseContext());
                List<Address> addresses = null;

                try {
                    // Getting a maximum of 3 Address that matches the input
                    // text
                    /**
                    addresses = geocoder.getFromLocationName(g, 3);
                    if (addresses != null && !addresses.equals(""))
                        search(addresses);
                    **/
                } catch (Exception e) {

                }
            }
        });

        final Button partners = (Button) findViewById(R.id.btn_partners);
        partners.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(MapsActivity.this, partners);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.popup_partners, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        //Toast.makeText(MapsActivity.this, "You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();

                        final Dialog dialog = new Dialog(MapsActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        // Include dialog.xml file
                        dialog.setContentView(R.layout.dialog_contact);
                        // set values for custom dialog components - text, image and button
                        dialog.show();

                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });

        final Button menumaps = (Button) findViewById(R.id.btn_menumaps);
        menumaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(MapsActivity.this, menumaps);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.popup_menumaps, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        Toast.makeText(MapsActivity.this, "You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });
    }


    public void check(){
        if (counter == -2) {
            //Toast.makeText(this, "TOLONG! Ada BEGAL!", Toast.LENGTH_SHORT).show();

            // Create custom dialog object
            final Dialog dialog = new Dialog(MapsActivity.this);
            // Include dialog.xml file
            dialog.setContentView(R.layout.dialog_help);
            dialog.setCancelable(false);
            // Set dialog title
            dialog.setTitle("BAHAYA!");

            // set values for custom dialog components - text, image and button

            dialog.show();

            Button declineButton = (Button) dialog.findViewById(R.id.btn_safe);
            // if decline button is clicked, close the custom dialog
            declineButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Close dialog
                    dialog.dismiss();
                }
            });

            counter = 0;
        } else if (counter == 1) {
            Toast.makeText(this, "TOLONG!!", Toast.LENGTH_SHORT).show();
            counter = 0;
        }
        else {
            //do nothing
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == android.view.KeyEvent.KEYCODE_VOLUME_DOWN) {
            counter--;
            check();
            //Toast.makeText(this, "Volume Down Pressed", Toast.LENGTH_SHORT).show();
            return true;
        }
        else if (keyCode == android.view.KeyEvent.KEYCODE_VOLUME_UP) {
            counter++;
            check();
            //Toast.makeText(this, "Volume Up Pressed", Toast.LENGTH_SHORT).show();
            return true;
        }
        else {
            return super.onKeyDown(keyCode, event);
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sby = new LatLng(-7.2754438, 112.6416433);
        mMap.addMarker(new MarkerOptions().position(sby).title("Marker in Surabaya"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sby));
    }
/**
    protected void search(List<Address> addresses) {

        Address address = (Address) addresses.get(0);
        home_long = address.getLongitude();
        home_lat = address.getLatitude();
        latLng = new LatLng(address.getLatitude(), address.getLongitude());

        addressText = String.format(
                "%s, %s",
                address.getMaxAddressLineIndex() > 0 ? address
                        .getAddressLine(0) : "", address.getCountryName());

        markerOptions = new MarkerOptions();

        markerOptions.position(latLng);
        markerOptions.title(addressText);

        map1.clear();
        map1.addMarker(markerOptions);
        map1.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        map1.animateCamera(CameraUpdateFactory.zoomTo(15));
        locationTv.setText("Latitude:" + address.getLatitude() + ", Longitude:"
                + address.getLongitude());


    }
 **/
}
